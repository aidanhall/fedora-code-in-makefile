#!/usr/bin/python

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

# Obtain the raw data from the file.
dataFile = open("some_data", "r")
rawData = dataFile.readlines()
print (rawData)
dataFile.close()

# Format the data in a nice way.
title = rawData[0][0:-1]
yAxisLabel = rawData[1][0:-1]
objects = rawData[2][0:-1].split()
performance = [ int(i) for i in rawData[3][0:-1].split() ]

print (title)
print (yAxisLabel)
print (objects)
print (performance)


# Display the data using matplotlib.

y_pos = np.arange(len(objects))

plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects)
plt.ylabel(yAxisLabel)
plt.title(title)

plt.show()
