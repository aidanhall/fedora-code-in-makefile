# Builds the final executable, depends on main.o.
outputData: main.o
	g++ -o outputData main.o

# Generates main.o.
main.o:
	g++ -c main.cpp

# Run make with clean after it to make it run this command,
# which removes any .o files,
# in stead of building the project.
# Use this before re-building if your source files change.
clean:
	rm -f *.o
