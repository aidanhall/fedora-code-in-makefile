# What this is.

* A submission for Google Code-in.

# What this isn't.

* A piece of software worthy of a license.

# Why I have included a license.

* The rules on the website suggest that submissions must be licensed.

<a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution 3.0 Unported License</a>.
