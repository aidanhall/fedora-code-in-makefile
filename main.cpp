#include <fstream>

int main()
{
    // Open the text file.
    std::ofstream data_file ( "some_data" );

    // Store the data in the text file.
    data_file << "Popularity of Pets\n";
    data_file << "Number of people with at least one\n";
    data_file << "Dog Rat Swallow Chameleon Penguin Gnu\n";
    data_file << "10 8 6 4 2 1\n";

    // Explicitly close the text file, to aid readability.
    data_file.close();

}
